# Projet VR Oculus Quest en dépôt

*Developed with Unreal Engine 4*

>To summarize, along with a database, navigable space is another key form of
>new media. It is already an accepted way for interacting with any type of data; an
>interface of computer games and motion simulators and, potentially, of any computer
>in general. Why does computer culture spatialize all representations and
>experiences (the library is replaced by cyberspace; narrative is equated with
>traveling through space; all kinds of data are rendered in three dimensions through
>computer visualization)? Shall we try to oppose this spatialization (i.e., what about
>time in new media?) And, finally, what are the aesthetics of navigation through
>virtual space?

[Lev Manovich - Navigable Space](http://manovich.net/index.php/projects/navigable-space)

## Qu'est-ce que c'est ?

Dans le cadre de mon [mémoire de recherche](https://github.com/etxetxe/DNSEP_Report_EESI_2020) portant sur la bifurcation, je m'intéresse à la conception et à la réalisation d'espaces navigable par le biais des outils de la réalité virtuelle. Ceci est le dépôt compilant toutes les itérations de mon projet, L'intitulé et le type d'expérience que je souhaite faire dans cet espace immersif n'est pas encore bien défini, les éléments et modes d'interaction au sein de cet univers restant à inventer.

## Comment ça marche ?

Vous êtes invités à dupliquer, forker, modifier le projet selon les termes de la LICENSE, l'idée étant pour moi de me familiariser avec les modalités interactionnelles du travail collaboratif asynchrone et petit-à-petit développer des réflexes de développeur.

### *En cours de rédaction ...*
